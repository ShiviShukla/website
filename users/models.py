from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models import Sum

class CustomUser(AbstractUser):
    age = models.PositiveIntegerField(null=True, blank=True)
    date_of_birth = models.DateTimeField(blank=True, null=True)
    current_city = models.CharField(max_length=200, blank=True, null=True)
    school = models.CharField(max_length=200, blank=True, null=True)
    college = models.CharField(max_length=200, blank=True, null=True)
    work = models.CharField(max_length=200, blank=True, null=True)
    follower = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='follow')

    def __str__(self):
        return self.username


"""
Task - Create models for the following entities

Book - title, price
Category - name
BookCategory - book_id, category_id
BookAuthor - book_id, author_id


A book can belong to one or more categories
A book can be written by one or more authors (users)


Create a Django project called library

Create a Django app called books and add it to INSTALLED_APPS

Create the above models in books/models.py file
Initialize the database
Migrate the database
"""


class Category(models.Model):
    name = models.TextField()

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.TextField(null=True)
    prize = models.IntegerField(null=True)
    categories = models.ManyToManyField(Category, related_name='books')
    authors = models.ManyToManyField(CustomUser, related_name='books')

    def __str__(self):
        return self.title
#
#
# class BookCategory(models.Model):
#     book_id = models.IntegerField()
#     category_id = models.IntegerField()
#
#
# class BookAuthor(models.Model):
#     book_id = models.IntegerField()
#     author_id = models.IntegerField()


"""
Create 10 users (usernames ranging from u1 to u10)


Create 40 books with a random price ranging between 10 to 100. (book names ranging from b1 to b40)


Create 10 categories. (names ranging from c1 to c10)


Add 3 random categories to each book

===========================
Add 3 random categories to each book


Add 2 random authors(users) to each book
"""
