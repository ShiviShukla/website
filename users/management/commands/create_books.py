from django.core.management.base import BaseCommand
from users.models import Book
from django.utils.crypto import get_random_string
import random


class Command(BaseCommand):
    help = 'Create random books'

    def add_arguments(self, parser):
        parser.add_argument('total', type=int, help='Indicates the number of books to be created')

    def handle(self, *args, **kwargs):
        total = kwargs['total']
        for i in range(total):
            Book(title=f"b{i}", prize=random.randint(10, 100)).save()
