from django.core.management.base import BaseCommand
from django.db.models import Sum, Max

from users.models import CustomUser, Book

import logging

l = logging.getLogger('django.db.backends')
l.setLevel(logging.DEBUG)
l.addHandler(logging.StreamHandler())


class Command(BaseCommand):
    help = 'Create random users'

    # def add_arguments(self, parser):
    #     parser.add_argument('total', type=int, help='Indicates the number of users to be created')

    def handle(self, *args, **kwargs):
        # CustomUser.objects.all()

        # 1. Fetch book having id = 1
        Book.objects.filter(id=100)    # list
        Book.objects.get(id=1)  # object

        # 2. Fetch books having ids in [1, 2, 3] in one query
        Book.objects.filter(id__in=[100, 101, 103])   # list

        # 3. Fetch all books belonging to category c1
        Book.objects.prefetch_related("categories").filter(categories__name="c1")

        # 4. How many books belong to category c2?
        Book.objects.prefetch_related("categories").filter(categories__name="c2").count()

        # 5. Which author has authored the most number of books?
        CustomUser.objects.all().order_by('-books')[0]   # author
        CustomUser.objects.all().order_by('-books')[0].books.count() # 12

        # 6. What is the total cost of all the books?
        Book.objects.aggregate(Sum('prize'))

        # 7. Which is the most expensive book?
        Book.objects.all().order_by('-prize')[0] # book

        # 8. Fetch all the books whose price exceeds 30
        Book.objects.filter(prize__gt=30).order_by('prize')

        # 9. Delete book having id=10
        Book.objects.get(id=10).delete()
