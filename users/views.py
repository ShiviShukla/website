from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView
from .forms import CustomUserCreationForm
from users.models import CustomUser
from django.http import JsonResponse, HttpResponse


class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    success_url = 'login'
    template_name = 'signup.html'


def follow_author(request):
    if request.is_ajax:
        if request.user.is_authenticated:
            author_id = request.POST.get('author_id', None)
            author = CustomUser.objects.filter(id=author_id).first()
            if author:
                if not author.follower.filter(id=request.user.id).exists():
                    author.follower.add(request.user)
                    author.save()
                    return JsonResponse({'result': True})
    return JsonResponse({'result': False})


# 'users/friend_follow/'
def follow(request):
    if request.method == "GET":
        users = CustomUser.objects.exclude(id=request.user.id)
        return render(request, 'users/users.html', {'users': users})

    if request.method == "POST":
        # import pdb
        # pdb.set_trace()
        user_id = request.POST.get('user_id', None)
        user = CustomUser.objects.filter(id=user_id).first()
        if user:
            if not user.follower.filter(id=user.id).exists():
                user.follower.add(request.user)
                user.save()
            return JsonResponse({'result': True})


# users/search/
def search(request):
    form_data = request.POST.get('data')
    users = CustomUser.objects.filter(username__icontains=form_data).all()
    return render(request, 'users/users.html', {'users': users})
