from django.urls import path

from .views import SignUpView, follow_author, follow, search

urlpatterns=[
    path('signup', SignUpView.as_view(), name='signup'),
    path('follow-author/', follow_author, name='follow_author'),
    path('friend_follow/', follow, name='follow'),
    path('search/', search, name='search'),
]
