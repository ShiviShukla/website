from django.urls import path
from .views import user_home, user_timeline, post_detail,like_post


urlpatterns=[
    path('home/', user_home, name='user_home'),
    path('timeline/<int:pk>', user_timeline, name='user_timeline'),
    path('detail/<int:post_id>', post_detail, name='post_detail'),
    path('like-post/', like_post, name='like_post'),

]
