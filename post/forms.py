from django import forms


class PostForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea, required=False)
    image = forms.ImageField(required=False)
    # image = forms.FileField()