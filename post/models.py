from django.conf import settings
from django.db import models
from django.urls import reverse


class Post(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    model_pic = models.ImageField(upload_to='pic_folder/', default='pic_folder/None/no-img.jpg')
    likes = models.IntegerField(default=0)

    def get_absolute_url(self):
        return reverse('article_detail', args=[str(self.id)])


class PostLike(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username
