from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import render, redirect
from post.forms import PostForm
from post.models import Post, PostLike
from django.http import JsonResponse

def user_home(request):
    if request.method == "POST":
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            text = form.cleaned_data['text']
            model_pic = form.cleaned_data['image']
            user_post = Post(
                author=request.user,
                text=text, model_pic=model_pic
            )
            user_post.save()
            return redirect('user_timeline', pk=request.user.pk)
    else:
        form = PostForm()
        posts = Post.objects.all()
        followers = ''
    return render(request, 'home.html', {'form': form, 'posts': posts})


def user_timeline(request, pk):
    if request.method == "POST":
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            text = form.cleaned_data['text']
            model_pic = form.cleaned_data['image']
            user_post = Post(
                author=request.user,
                text=text, model_pic=model_pic
            )
            user_post.save()
            return redirect('user_timeline', pk=request.user.pk)
    posts = Post.objects.filter(author=request.user)
    form = PostForm()
    return render(request, 'post/timeline.html', {'posts': posts, 'form': form})


def post_detail(request, post_id):
    post = Post.objects.filter(id=int(post_id)).first()
    return render(request, 'post/post_detail.html', {'post': post})


def like_post(request):
    if request.is_ajax:
        if request.user.is_authenticated:
            post_id = request.POST.get('post_id', None)
            post = Post.objects.filter(pk=int(post_id)).first()
            # import pdb
            # pdb.set_trace()
            postlike = post.postlike_set.filter(user=request.user)
            if not postlike:
                post.likes += 1
                post.save()
                postlike=PostLike(user=request.user,post=post)
                postlike.save()
                return JsonResponse({'result': True, 'likes':post.likes})

            else:
                PostLike.objects.filter(id=request.user.id).delete()
                post.likes -= 1
                post.save()
                return JsonResponse({'result': True, 'likes':post.likes})
"""

class PostLike(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

"""